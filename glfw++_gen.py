import clang.cindex as libclang

import os
import re

C2SC = re.compile(r'(?<!^)(?=[A-Z])')


class FuncParm:
    def __init__(self, decl):
        self.name = decl.spelling
        self.type = decl.type.spelling.replace(' *', '*')

    def __repr__(self):
        return "%s %s" % (self.type, self.name)


class Func:
    def __init__(self, decl):
        self.name = C2SC.sub('_', decl.mangled_name.replace('glfw', '')).lower()
        self.comment = decl.raw_comment
        self.return_type = decl.result_type.spelling.replace(' *', '*')
        self.arguments_type = []
        for arg in decl.get_arguments():
            self.arguments_type.append(FuncParm(arg))

    def __repr__(self):
        return "%s %s(%s)" % (self.return_type, self.name, ', '.join([str(a) for a in self.arguments_type]))


if __name__ == "__main__":
    index = libclang.Index.create()
    commands = libclang.CompilationDatabase.fromDirectory('build')
    command = commands.getCompileCommands('glfw_ast.c')[0]
    print("cd '%s'" % (command.directory))
    os.chdir(command.directory)
    args = []
    for arg_i, arg in enumerate(command.arguments):
        if arg_i == 0:
            print("Compiler: %s" % arg)
        elif arg.startswith('-I'):
            args.append(arg)
    args.append('-pthread')
    args.append('-std=c99')
    tu = index.parse("../tests/%s" % command.filename, args)

    ast_root = tu.cursor

    func_reg = []

    # First pass: find function declarations we are interested in.
    for decl in ast_root.get_children():
        if decl.kind == libclang.CursorKind.FUNCTION_DECL:
            fnc_name = decl.mangled_name
            # Filtering based on name.
            if fnc_name.startswith('glfw'):
                fnc = Func(decl)
                func_reg.append(fnc)
                print(fnc.comment, fnc, sep='\n')
