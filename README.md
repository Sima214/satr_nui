# SATR Native UI

Core module of Satr

## Overview

Provides:

- GLFW
- Vulkan

Contains:

- GLFW++ bindings
- Vulkan C++ class bindings.
