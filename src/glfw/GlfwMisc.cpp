#include "GlfwMisc.hpp"

namespace satr::nui {

Image::Image(glm::ivec2 size) : _size(size), _data(nullptr) {
    if (size.x <= 0 || size.y <= 0) {
        throw std::invalid_argument("satr::nui::Image::Image(size <= 0)");
    }
    _data = new RGBA[size.x * size.y];
}

Image::~Image() {
    if (_data != nullptr) {
        delete[] _data;
    }
}

Image::RGBA& Image::operator[](glm::ivec2 p) {
    if (p.x < 0 || p.y < 0) {
        throw std::invalid_argument("satr::nui::Image[](p < 0)");
    }
    if (p.x >= _size.x || p.y >= _size.y) {
        throw std::invalid_argument("satr::nui::Image[](p >= _size)");
    }
    size_t i = p.y * _size.x + p.y;
    return _data[i];
}

Cursor::Cursor() : _handle(nullptr) {}

Cursor::Cursor(GLFWcursor* handle) : _handle(handle) {}

Cursor::~Cursor() {
    if (_handle != nullptr) {
        glfwDestroyCursor(_handle);
    }
}

}  // namespace satr::nui
