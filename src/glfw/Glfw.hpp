#ifndef SATR_SPEC_NUI_GLFW_HPP
#define SATR_SPEC_NUI_GLFW_HPP
/**
 * @file
 * @brief C++ RAII style GLFW bindings(groups: init, ...).
 */

#include <events/DispatchTable.hpp>
#include <glm/vec2.hpp>
#include <meta/Utils.hpp>
#include <vulkan/vulkan.hpp>

#include <cstddef>
#include <cstdint>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

#include <GLFW/glfw3.h>

namespace satr::nui {

class GLFW;
class Image;
class Cursor;
class Monitor;
class WindowHints;
class Window;

class glfw_error : public std::runtime_error {
   protected:
    int code;
    std::string description;

   public:
    glfw_error();

    glfw_error(int glfw_code, const char* glfw_desc);

    virtual char const* what() const noexcept override;

    static void check();

   private:
    void set_error_msg(int glfw_code, const char* glfw_desc);
};

struct Version {
   private:
    const int _major = -1;
    const int _minor = -1;
    const int _revision = -1;

    Version(int major, int minor, int revision) :
        _major(major), _minor(minor), _revision(revision) {}

    friend class GLFW;
    friend class Window;

   public:
    int get_major() const {
        return _major;
    }
    int get_minor() const {
        return _minor;
    }
    int get_revision() const {
        return _revision;
    }

    static Version get_compiled_version();

    static Version get_runtime_version();
};

template<typename CTy, typename CTr>
std::basic_ostream<CTy, CTr>& operator<<(std::basic_ostream<CTy, CTr>& o, const Version& v) {
    o << v.get_major() << '.' << v.get_minor() << '.' << v.get_revision();
    return o;
}

enum class GLFWPlatform {
    Any = GLFW_ANY_PLATFORM,
    Win32 = GLFW_PLATFORM_WIN32,
    Cocoa = GLFW_PLATFORM_COCOA,
    Wayland = GLFW_PLATFORM_WAYLAND,
    X11 = GLFW_PLATFORM_X11,
    Null = GLFW_PLATFORM_NULL,
};

class GLFWHints {
   private:
    int platform = -1;
    int joystick_hat_buttons = -1;
    int cocoa_chdir_resources = -1;
    int cocoa_menubar = -1;

   public:
    GLFWHints() = default;

    void set_platform(GLFWPlatform v) {
        platform = static_cast<int>(v);
    }
    void set_joystick_hat_buttons(bool v) {
        joystick_hat_buttons = v ? GLFW_TRUE : GLFW_FALSE;
    }
    void set_cocoa_chdir_resources(bool v) {
        cocoa_chdir_resources = v ? GLFW_TRUE : GLFW_FALSE;
    }
    void set_cocoa_menubar(bool v) {
        cocoa_menubar = v ? GLFW_TRUE : GLFW_FALSE;
    }

   private:
    void apply();
    friend class GLFW;
};

class GLFW : public spec::INonCopyable {
   public:
    using monitor_cb = void(Monitor&, bool connected);

   private:
    spec::DispatchTable<monitor_cb> monitor_dt;

    GLFW();

   public:
    static GLFW& get_instance();
    /**
     * Create an instance with \p hints.
     * If get_instance has already been called,
     * then \p hints has no effect.
     */
    static GLFW& get_instance(GLFWHints& hints) {
        hints.apply();
        return get_instance();
    }

    ~GLFW();

    // preinit
    static bool is_platform_supported(GLFWPlatform platform);
    static void init_vulkan_loader(PFN_vkGetInstanceProcAddr loader);

    // init
    static Version get_compiled_version();
    Version get_runtime_version();
    GLFWPlatform get_platform();

    // vulkan
    bool vulkan_supported();
    std::vector<const char*> get_required_instance_extensions();
    void* get_instance_proc_address(::vk::Instance& instance, const char* procname);
    void* get_instance_proc_address(::vk::Instance& instance, const std::string& procname) {
        return get_instance_proc_address(instance, procname.c_str());
    }
    bool get_physical_device_presentation_support(::vk::Instance instance,
                                                  ::vk::PhysicalDevice device,
                                                  uint32_t queuefamilyindex);

    // input
    double get_time();
    void set_time(double time);
    uint64_t get_timer_value();
    uint64_t get_timer_frequency();

    // monitor
    std::vector<Monitor> get_monitors();
    std::optional<Monitor> get_primary_monitor();
    size_t register_monitor_callback(std::function<monitor_cb> callback);
    void unregister_monitor_callback(size_t uid);

    // window
    Window create_window(glm::ivec2 size, const char* title, Monitor& monitor,
                         const WindowHints& hints, Window& share);
    Window create_window(glm::ivec2 size, const char* title, Monitor& monitor,
                         const WindowHints& hints);
    Window create_window(glm::ivec2 size, const char* title, Monitor& monitor, Window& share);
    Window create_window(glm::ivec2 size, const char* title, const WindowHints& hints,
                         Window& share);
    Window create_window(glm::ivec2 size, const char* title, Monitor& monitor);
    Window create_window(glm::ivec2 size, const char* title, const WindowHints& hints);
    Window create_window(glm::ivec2 size, const char* title, Window& share);
    Window create_window(glm::ivec2 size, const char* title);

    Window create_window(glm::ivec2 size, const std::string& title, Monitor& monitor,
                         const WindowHints& hints, Window& share);
    Window create_window(glm::ivec2 size, const std::string& title, Monitor& monitor,
                         const WindowHints& hints);
    Window create_window(glm::ivec2 size, const std::string& title, Monitor& monitor,
                         Window& share);
    Window create_window(glm::ivec2 size, const std::string& title, const WindowHints& hints,
                         Window& share);
    Window create_window(glm::ivec2 size, const std::string& title, Monitor& monitor);
    Window create_window(glm::ivec2 size, const std::string& title, const WindowHints& hints);
    Window create_window(glm::ivec2 size, const std::string& title, Window& share);
    Window create_window(glm::ivec2 size, const std::string& title);

   protected:
    static void on_monitor(GLFWmonitor* mon, int status);
};

}  // namespace satr::nui

#endif /*SATR_SPEC_NUI_GLFW_HPP*/