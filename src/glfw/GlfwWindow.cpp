#include "GlfwWindow.hpp"

#include <GlfwMonitor.hpp>
#include <trace/Trace.hpp>

#include <stdexcept>

#include <macros.h>

namespace satr::nui {

void WindowHints::apply() const {
    if (resizable != -1) {
        glfwWindowHint(GLFW_RESIZABLE, resizable);
    }
    if (visible != -1) {
        glfwWindowHint(GLFW_VISIBLE, visible);
    }
    if (decorated != -1) {
        glfwWindowHint(GLFW_DECORATED, decorated);
    }
    if (focused != -1) {
        glfwWindowHint(GLFW_FOCUSED, focused);
    }
    if (auto_iconify != -1) {
        glfwWindowHint(GLFW_AUTO_ICONIFY, auto_iconify);
    }
    if (floating != -1) {
        glfwWindowHint(GLFW_FLOATING, floating);
    }
    if (maximized != -1) {
        glfwWindowHint(GLFW_MAXIMIZED, maximized);
    }
    if (center_cursor != -1) {
        glfwWindowHint(GLFW_CENTER_CURSOR, center_cursor);
    }
    if (transparent_framebuffer != -1) {
        glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, transparent_framebuffer);
    }
    if (focus_on_show != -1) {
        glfwWindowHint(GLFW_FOCUS_ON_SHOW, focus_on_show);
    }
    if (scale_to_monitor != -1) {
        glfwWindowHint(GLFW_SCALE_TO_MONITOR, scale_to_monitor);
    }
    if (mouse_passthrough != -1) {
        glfwWindowHint(GLFW_MOUSE_PASSTHROUGH, mouse_passthrough);
    }

    glfwWindowHint(GLFW_RED_BITS, red_bits);
    glfwWindowHint(GLFW_GREEN_BITS, green_bits);
    glfwWindowHint(GLFW_BLUE_BITS, blue_bits);
    glfwWindowHint(GLFW_ALPHA_BITS, alpha_bits);
    glfwWindowHint(GLFW_DEPTH_BITS, depth_bits);
    glfwWindowHint(GLFW_STENCIL_BITS, stencil_bits);
    glfwWindowHint(GLFW_ACCUM_RED_BITS, accum_red_bits);
    glfwWindowHint(GLFW_ACCUM_GREEN_BITS, accum_green_bits);
    glfwWindowHint(GLFW_ACCUM_BLUE_BITS, accum_blue_bits);
    glfwWindowHint(GLFW_ACCUM_ALPHA_BITS, accum_alpha_bits);
    glfwWindowHint(GLFW_AUX_BUFFERS, aux_buffers);

    glfwWindowHint(GLFW_SAMPLES, samples);
    glfwWindowHint(GLFW_REFRESH_RATE, refresh_rate);

    if (stereo != -1) {
        glfwWindowHint(GLFW_STEREO, stereo);
    }
    if (srgb_capable != -1) {
        glfwWindowHint(GLFW_SRGB_CAPABLE, srgb_capable);
    }
    if (doublebuffer != -1) {
        glfwWindowHint(GLFW_DOUBLEBUFFER, doublebuffer);
    }

    glfwWindowHint(GLFW_CLIENT_API, client_api);
    glfwWindowHint(GLFW_CONTEXT_CREATION_API, context_creation_api);
    if (context_version_major != -1) {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, context_version_major);
    }
    if (context_version_minor != -1) {
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, context_version_minor);
    }
    if (opengl_forward_compat != -1) {
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, opengl_forward_compat);
    }
    if (opengl_debug_context != -1) {
        glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, opengl_debug_context);
    }
    glfwWindowHint(GLFW_OPENGL_PROFILE, opengl_profile);
    glfwWindowHint(GLFW_CONTEXT_ROBUSTNESS, context_robustness);
    glfwWindowHint(GLFW_CONTEXT_RELEASE_BEHAVIOR, context_release_behavior);
    if (context_no_error != -1) {
        glfwWindowHint(GLFW_CONTEXT_NO_ERROR, context_no_error);
    }
    if (cocoa_retina_framebuffer != -1) {
        glfwWindowHint(GLFW_COCOA_RETINA_FRAMEBUFFER, cocoa_retina_framebuffer);
    }
    glfwWindowHintString(GLFW_COCOA_FRAME_NAME, cocoa_frame_name.c_str());
    if (cocoa_graphics_switching != -1) {
        glfwWindowHint(GLFW_COCOA_GRAPHICS_SWITCHING, cocoa_graphics_switching);
    }
    if (win32_keyboard_menu != -1) {
        glfwWindowHint(GLFW_WIN32_KEYBOARD_MENU, win32_keyboard_menu);
    }
    glfwWindowHintString(GLFW_X11_CLASS_NAME, x11_class_name.c_str());
    glfwWindowHintString(GLFW_X11_INSTANCE_NAME, x11_instance_name.c_str());
    glfw_error::check();
}

void WindowHints::reset() const {
    glfwDefaultWindowHints();
    glfw_error::check();
}

Window::Modifiers::Modifiers(int mods) :
    shift(MASK_TEST(mods, GLFW_MOD_SHIFT)), control(MASK_TEST(mods, GLFW_MOD_CONTROL)),
    alt(MASK_TEST(mods, GLFW_MOD_ALT)), super(MASK_TEST(mods, GLFW_MOD_SUPER)),
    caps_lock(MASK_TEST(mods, GLFW_MOD_CAPS_LOCK)),
    num_lock(MASK_TEST(mods, GLFW_MOD_NUM_LOCK)) {}

Window::Window(glm::ivec2 size, const char* title, Monitor* monitor, Window* share) :
    _handle(glfwCreateWindow(size.x, size.y, title,
                             monitor != nullptr ? monitor->_handle : nullptr,
                             share != nullptr ? share->_handle : nullptr)),
    _user_data(nullptr) {
    glfw_error::check();
    _crossreference();

    glfwSetWindowPosCallback(_handle, _position_callback);
    glfwSetWindowSizeCallback(_handle, _size_callback);
    glfwSetWindowCloseCallback(_handle, _close_callback);
    glfwSetWindowRefreshCallback(_handle, _refresh_callback);
    glfwSetWindowFocusCallback(_handle, _focus_callback);
    glfwSetWindowIconifyCallback(_handle, _iconify_callback);
    glfwSetWindowMaximizeCallback(_handle, _maximize_callback);
    glfwSetFramebufferSizeCallback(_handle, _framebuffer_size_callback);
    glfwSetWindowContentScaleCallback(_handle, _content_scale_callback);

    glfwSetKeyCallback(_handle, _key_callback);
    glfwSetCharCallback(_handle, _char_callback);
    glfwSetCharModsCallback(_handle, _charmods_callback);
    glfwSetMouseButtonCallback(_handle, _mouse_button_callback);
    glfwSetCursorPosCallback(_handle, _cursor_pos_callback);
    glfwSetCursorEnterCallback(_handle, _cursor_enter_callback);
    glfwSetScrollCallback(_handle, _scroll_callback);
    glfwSetDropCallback(_handle, _drop_callback);

    glfw_error::check();
    spec::trace("glfw::window::create(", _handle, ")");
}

void Window::_crossreference() {
    glfwSetWindowUserPointer(_handle, this);
}

Window* Window::_retrieve_crossreference(GLFWwindow* o) {
    return static_cast<Window*>(glfwGetWindowUserPointer(o));
}

Window::~Window() {
    if (_handle != nullptr) {
        spec::trace("glfw::window::destroy(", _handle, ")");
        glfwDestroyWindow(_handle);
        glfw_error::check();
    }
}

bool Window::should_close() {
    return glfwWindowShouldClose(_handle);
}
void Window::set_should_close(bool v) {
    glfwSetWindowShouldClose(_handle, v);
}

void Window::set_title(const char* title) {
    glfwSetWindowTitle(_handle, title);
    glfw_error::check();
}
void Window::set_title(const std::string& title) {
    set_title(title.c_str());
}

void Window::set_icons(std::vector<Image> images) {
    int n = images.size();
    if (size_t(n) != images.size()) {
        throw std::overflow_error("satr::nui::Window::set_icons(images::size overflow)");
    }
    GLFWimage* pass = new GLFWimage[n];
    for (int i = 0; i < n; i++) {
        pass[i] = static_cast<GLFWimage>(images[i]);
    }
    delete[] pass;
}

glm::ivec2 Window::get_pos() {
    glm::ivec2 pos;
    glfwGetWindowPos(_handle, &pos.x, &pos.y);
    glfw_error::check();
    return pos;
}
void Window::set_pos(glm::ivec2 pos) {
    glfwSetWindowPos(_handle, pos.x, pos.y);
    glfw_error::check();
}

glm::ivec2 Window::get_size() {
    glm::ivec2 size;
    glfwGetWindowSize(_handle, &size.x, &size.y);
    glfw_error::check();
    return size;
}
void Window::set_size_limits(glm::ivec2 min_size, glm::ivec2 max_size) {
    glfwSetWindowSizeLimits(_handle, min_size.x, min_size.y, max_size.x, max_size.y);
    glfw_error::check();
}
void Window::set_aspect_ratio(int numer, int denom) {
    glfwSetWindowAspectRatio(_handle, numer, denom);
    glfw_error::check();
}
void Window::set_size(glm::ivec2 size) {
    glfwSetWindowSize(_handle, size.x, size.y);
    glfw_error::check();
}
glm::ivec2 Window::get_framebuffer_size() {
    glm::ivec2 size;
    glfwGetFramebufferSize(_handle, &size.x, &size.y);
    glfw_error::check();
    return size;
}
glm::ivec4 Window::get_frame_size() {
    glm::ivec4 size;
    glfwGetWindowFrameSize(_handle, &size.x, &size.y, &size.z, &size.w);
    glfw_error::check();
    return size;
}
glm::vec2 Window::get_content_scale() {
    glm::vec2 scale;
    glfwGetWindowContentScale(_handle, &scale.x, &scale.y);
    glfw_error::check();
    return scale;
}

float Window::get_opacity() {
    float opacity = glfwGetWindowOpacity(_handle);
    glfw_error::check();
    return opacity;
}
void Window::set_opacity(float opacity) {
    glfwSetWindowOpacity(_handle, opacity);
    glfw_error::check();
}

bool Window::get_focused() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_FOCUSED);
    glfw_error::check();
    return v != GLFW_FALSE;
}
bool Window::get_iconified() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_ICONIFIED);
    glfw_error::check();
    return v != GLFW_FALSE;
}
bool Window::get_maximized() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_MAXIMIZED);
    glfw_error::check();
    return v != GLFW_FALSE;
}
bool Window::get_hovered() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_HOVERED);
    glfw_error::check();
    return v != GLFW_FALSE;
}
bool Window::get_visible() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_VISIBLE);
    glfw_error::check();
    return v != GLFW_FALSE;
}
bool Window::get_resizable() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_RESIZABLE);
    glfw_error::check();
    return v != GLFW_FALSE;
}
bool Window::get_decorated() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_DECORATED);
    glfw_error::check();
    return v != GLFW_FALSE;
}
bool Window::get_auto_iconify() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_AUTO_ICONIFY);
    glfw_error::check();
    return v != GLFW_FALSE;
}
bool Window::get_floating() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_FLOATING);
    glfw_error::check();
    return v != GLFW_FALSE;
}
bool Window::get_transparent_framebuffer() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_TRANSPARENT_FRAMEBUFFER);
    glfw_error::check();
    return v != GLFW_FALSE;
}
bool Window::get_focus_on_show() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_FOCUS_ON_SHOW);
    glfw_error::check();
    return v != GLFW_FALSE;
}
WindowHints::ClientApi Window::get_client_api() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_CLIENT_API);
    glfw_error::check();
    return WindowHints::ClientApi(v);
}
WindowHints::ContextCreationApi Window::get_context_creation_api() {
    auto v = glfwGetWindowAttrib(_handle, GLFW_CONTEXT_CREATION_API);
    glfw_error::check();
    return WindowHints::ContextCreationApi(v);
}
Version Window::get_context_version() {
    auto major = glfwGetWindowAttrib(_handle, GLFW_CONTEXT_VERSION_MAJOR);
    auto minor = glfwGetWindowAttrib(_handle, GLFW_CONTEXT_VERSION_MINOR);
    auto revision = glfwGetWindowAttrib(_handle, GLFW_CONTEXT_REVISION);
    glfw_error::check();
    return Version(major, minor, revision);
}

void Window::set_decorated(bool v) {
    int c = v ? GLFW_TRUE : GLFW_FALSE;
    glfwSetWindowAttrib(_handle, GLFW_DECORATED, c);
    glfw_error::check();
}
void Window::set_resizable(bool v) {
    int c = v ? GLFW_TRUE : GLFW_FALSE;
    glfwSetWindowAttrib(_handle, GLFW_RESIZABLE, c);
    glfw_error::check();
}
void Window::set_floating(bool v) {
    int c = v ? GLFW_TRUE : GLFW_FALSE;
    glfwSetWindowAttrib(_handle, GLFW_FLOATING, c);
    glfw_error::check();
}
void Window::set_auto_iconify(bool v) {
    int c = v ? GLFW_TRUE : GLFW_FALSE;
    glfwSetWindowAttrib(_handle, GLFW_AUTO_ICONIFY, c);
    glfw_error::check();
}
void Window::set_focus_on_show(bool v) {
    int c = v ? GLFW_TRUE : GLFW_FALSE;
    glfwSetWindowAttrib(_handle, GLFW_FOCUS_ON_SHOW, c);
    glfw_error::check();
}

void Window::iconify() {
    glfwIconifyWindow(_handle);
    glfw_error::check();
}
void Window::restore() {
    glfwRestoreWindow(_handle);
    glfw_error::check();
}
void Window::maximize() {
    glfwMaximizeWindow(_handle);
    glfw_error::check();
}
void Window::show() {
    glfwShowWindow(_handle);
    glfw_error::check();
}
void Window::hide() {
    glfwHideWindow(_handle);
    glfw_error::check();
}
void Window::focus() {
    glfwFocusWindow(_handle);
    glfw_error::check();
}
void Window::request_attention() {
    glfwRequestWindowAttention(_handle);
    glfw_error::check();
}

std::optional<Monitor> Window::get_monitor() {
    GLFWmonitor* p = glfwGetWindowMonitor(_handle);
    glfw_error::check();
    if (p != nullptr) {
        return Monitor(p);
    }
    return {};
}
void Window::set_monitor(Monitor& monitor, glm::ivec2 pos, glm::ivec2 size, int refresh_rate) {
    glfwSetWindowMonitor(_handle, monitor._handle, pos.x, pos.y, size.x, size.y, refresh_rate);
    glfw_error::check();
}

void Window::set_windowed(glm::ivec2 pos, glm::ivec2 size) {
    glfwSetWindowMonitor(_handle, nullptr, pos.x, pos.y, size.x, size.y, GLFW_DONT_CARE);
    glfw_error::check();
}

size_t Window::register_position_callback(std::function<pos_cb> callback) {
    return _pos_dt.register_callback(callback);
}
void Window::unregister_position_callback(size_t uid) {
    _pos_dt.unregister_callback(uid);
}

size_t Window::register_size_callback(std::function<size_cb> callback) {
    return _size_dt.register_callback(callback);
}
void Window::unregister_size_callback(size_t uid) {
    _size_dt.unregister_callback(uid);
}

size_t Window::register_close_callback(std::function<close_cb> callback) {
    return _close_dt.register_callback(callback);
}
void Window::unregister_close_callback(size_t uid) {
    _close_dt.unregister_callback(uid);
}

size_t Window::register_refresh_callback(std::function<refresh_cb> callback) {
    return _refresh_dt.register_callback(callback);
}
void Window::unregister_refresh_callback(size_t uid) {
    _refresh_dt.unregister_callback(uid);
}

size_t Window::register_focus_callback(std::function<focus_cb> callback) {
    return _focus_dt.register_callback(callback);
}
void Window::unregister_focus_callback(size_t uid) {
    _focus_dt.unregister_callback(uid);
}

size_t Window::register_iconify_callback(std::function<iconify_cb> callback) {
    return _iconify_dt.register_callback(callback);
}
void Window::unregister_iconify_callback(size_t uid) {
    _iconify_dt.unregister_callback(uid);
}

size_t Window::register_maximize_callback(std::function<maximize_cb> callback) {
    return _maximize_dt.register_callback(callback);
}
void Window::unregister_maximize_callback(size_t uid) {
    _maximize_dt.unregister_callback(uid);
}

size_t Window::register_framebuffer_size_callback(std::function<fbsize_cb> callback) {
    return _fbsize_dt.register_callback(callback);
}
void Window::unregister_framebuffer_size_callback(size_t uid) {
    _fbsize_dt.unregister_callback(uid);
}

size_t Window::register_content_scale_callback(std::function<scale_cb> callback) {
    return _scale_dt.register_callback(callback);
}
void Window::unregister_content_scale_callback(size_t uid) {
    _scale_dt.unregister_callback(uid);
}

void Window::poll_events() {
    glfwPollEvents();
    glfw_error::check();
}
void Window::wait_events() {
    glfwWaitEvents();
    glfw_error::check();
}
void Window::wait_events_timeout(double timeout) {
    glfwWaitEventsTimeout(timeout);
    glfw_error::check();
}
void Window::post_empty_event() {
    glfwPostEmptyEvent();
    glfw_error::check();
}

Window::CursorMode Window::get_input_cursor_mode() {
    return Window::CursorMode(glfwGetInputMode(_handle, GLFW_CURSOR));
}
void Window::set_input_cursor_mode(CursorMode mode) {
    glfwSetInputMode(_handle, GLFW_CURSOR, mode);
    glfw_error::check();
}
bool Window::get_input_sticky_keys_mode() {
    return glfwGetInputMode(_handle, GLFW_STICKY_KEYS);
}
void Window::set_input_sticky_keys_mode(bool mode) {
    int v = mode ? GLFW_TRUE : GLFW_FALSE;
    glfwSetInputMode(_handle, GLFW_STICKY_KEYS, v);
    glfw_error::check();
}
bool Window::get_input_sticky_mouse_buttons_mode() {
    return glfwGetInputMode(_handle, GLFW_STICKY_MOUSE_BUTTONS);
}
void Window::set_input_sticky_mouse_buttons_mode(bool mode) {
    int v = mode ? GLFW_TRUE : GLFW_FALSE;
    glfwSetInputMode(_handle, GLFW_STICKY_MOUSE_BUTTONS, v);
    glfw_error::check();
}
bool Window::get_input_lock_keys_mods_mode() {
    return glfwGetInputMode(_handle, GLFW_LOCK_KEY_MODS);
}
void Window::set_input_lock_keys_mods_mode(bool mode) {
    int v = mode ? GLFW_TRUE : GLFW_FALSE;
    glfwSetInputMode(_handle, GLFW_LOCK_KEY_MODS, v);
    glfw_error::check();
}
bool Window::get_input_raw_mouse_motion_mode() {
    return glfwGetInputMode(_handle, GLFW_RAW_MOUSE_MOTION);
}
void Window::set_input_raw_mouse_motion_mode(bool mode) {
    int v = mode ? GLFW_TRUE : GLFW_FALSE;
    glfwSetInputMode(_handle, GLFW_RAW_MOUSE_MOTION, v);
    glfw_error::check();
}
bool Window::raw_mouse_motion_supported() {
    return glfwRawMouseMotionSupported() == GLFW_FALSE;
}

glm::dvec2 Window::get_cursor_pos() {
    glm::dvec2 r;
    glfwGetCursorPos(_handle, &r.x, &r.y);
    glfw_error::check();
    return r;
}
void Window::set_cursor_pos(glm::dvec2 pos) {
    glfwSetCursorPos(_handle, pos.x, pos.y);
    glfw_error::check();
}

std::optional<Cursor> Window::create_cursor(const Image& image, glm::ivec2 hotspot) {
    GLFWimage img = static_cast<GLFWimage>(image);
    GLFWcursor* c = glfwCreateCursor(&img, hotspot.x, hotspot.y);
    glfw_error::check();
    if (c != nullptr) {
        return Cursor(c);
    }
    return {};
}
std::optional<Cursor> Window::create_cursor(Cursor::StandardCursors shape) {
    GLFWcursor* c = glfwCreateStandardCursor(shape);
    glfw_error::check();
    if (c != nullptr) {
        return Cursor(c);
    }
    return {};
}
void Window::set_cursor(Cursor& cursor) {
    glfwSetCursor(_handle, cursor._handle);
    glfw_error::check();
}

Window::ButtonState Window::get_mouse_button(MouseButtons button) {
    int state = glfwGetMouseButton(_handle, button);
    glfw_error::check();
    return Window::ButtonState(state);
}

const char* Window::get_key_name(Keys key, int scancode) {
    const char* s = glfwGetKeyName(key, scancode);
    glfw_error::check();
    return s;
}
int Window::get_key_scancode(Keys key) {
    int scancode = glfwGetKeyScancode(key);
    glfw_error::check();
    return scancode;
}
Window::ButtonState Window::get_key(Keys key) {
    int state = glfwGetKey(_handle, key);
    glfw_error::check();
    return Window::ButtonState(state);
}

void Window::set_clipboard_string(const char* string) {
    glfwSetClipboardString(_handle, string);
    glfw_error::check();
}
const char* Window::get_clipboard_string() {
    const char* s = glfwGetClipboardString(_handle);
    glfw_error::check();
    return s;
}

size_t Window::register_key_callback(std::function<key_cb> cb) {
    return _key_dt.register_callback(cb);
}
void Window::unregister_key_callback(size_t uid) {
    _key_dt.unregister_callback(uid);
}
size_t Window::register_char_callback(std::function<char_cb> cb) {
    return _char_dt.register_callback(cb);
}
void Window::unregister_char_callback(size_t uid) {
    _char_dt.unregister_callback(uid);
}
size_t Window::register_charmods_callback(std::function<charmods_cb> cb) {
    return _charmods_dt.register_callback(cb);
}
void Window::unregister_charmods_callback(size_t uid) {
    _charmods_dt.unregister_callback(uid);
}
size_t Window::register_mouse_button_callback(std::function<mouse_button_cb> cb) {
    return _mouse_button_dt.register_callback(cb);
}
void Window::unregister_mouse_button_callback(size_t uid) {
    _mouse_button_dt.unregister_callback(uid);
}
size_t Window::register_cursor_pos_callback(std::function<cursor_pos_cb> cb) {
    return _cursor_pos_dt.register_callback(cb);
}
void Window::unregister_cursor_pos_callback(size_t uid) {
    _cursor_pos_dt.unregister_callback(uid);
}
size_t Window::register_cursor_enter_callback(std::function<cursor_enter_cb> cb) {
    return _cursor_enter_dt.register_callback(cb);
}
void Window::unregister_cursor_enter_callback(size_t uid) {
    _cursor_enter_dt.unregister_callback(uid);
}
size_t Window::register_scroll_callback(std::function<scroll_cb> cb) {
    return _scroll_dt.register_callback(cb);
}
void Window::unregister_scroll_callback(size_t uid) {
    _scroll_dt.unregister_callback(uid);
}
size_t Window::register_drop_callback(std::function<drop_cb> cb) {
    return _drop_dt.register_callback(cb);
}
void Window::unregister_drop_callback(size_t uid) {
    _drop_dt.unregister_callback(uid);
}

void Window::make_context_current() {
    glfwMakeContextCurrent(_handle);
    glfw_error::check();
}
void Window::detach_context_current() {
    glfwMakeContextCurrent(nullptr);
    glfw_error::check();
}
Window* Window::get_current_context() {
    GLFWwindow* w = glfwGetCurrentContext();
    glfw_error::check();
    if (w == nullptr) {
        return nullptr;
    }
    Window* obj = _retrieve_crossreference(w);
    return obj;
}
void Window::swap_buffers() {
    glfwSwapBuffers(_handle);
    glfw_error::check();
}
void Window::swap_interval(int interval) {
    glfwSwapInterval(interval);
    glfw_error::check();
}
bool Window::extension_supported(const char* extension) {
    int v = glfwExtensionSupported(extension);
    glfw_error::check();
    return v != GLFW_FALSE;
}

std::pair<::vk::Result, ::vk::SurfaceKHR> Window::create_window_surface(
     ::vk::Instance& instance, const ::vk::AllocationCallbacks& allocator) {
    VkInstance cinstance = instance;
    const VkAllocationCallbacks callocator = static_cast<VkAllocationCallbacks>(allocator);
    VkSurfaceKHR csurface;
    ::vk::Result r = static_cast<::vk::Result>(
         glfwCreateWindowSurface(cinstance, _handle, &callocator, &csurface));
    glfw_error::check();
    ::vk::SurfaceKHR surface = csurface;
    return {r, surface};
}

std::pair<::vk::Result, ::vk::SurfaceKHR> Window::create_window_surface(
     ::vk::Instance& instance) {
    VkInstance cinstance = instance;
    VkSurfaceKHR csurface;
    ::vk::Result r = static_cast<::vk::Result>(
         glfwCreateWindowSurface(cinstance, _handle, nullptr, &csurface));
    glfw_error::check();
    ::vk::SurfaceKHR surface = csurface;
    return {r, surface};
}

void Window::_position_callback(GLFWwindow* o, int x, int y) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        glm::ivec2 pos(x, y);
        obj->_pos_dt(*obj, pos);
    }
}
void Window::_size_callback(GLFWwindow* o, int w, int h) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        glm::ivec2 size(w, h);
        obj->_size_dt(*obj, size);
    }
}
void Window::_close_callback(GLFWwindow* o) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        obj->_close_dt(*obj);
    }
}
void Window::_refresh_callback(GLFWwindow* o) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        obj->_refresh_dt(*obj);
    }
}
void Window::_focus_callback(GLFWwindow* o, int status) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        bool v = status == GLFW_TRUE;
        obj->_focus_dt(*obj, v);
    }
}
void Window::_iconify_callback(GLFWwindow* o, int status) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        bool v = status == GLFW_TRUE;
        obj->_iconify_dt(*obj, v);
    }
}
void Window::_maximize_callback(GLFWwindow* o, int status) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        bool v = status == GLFW_TRUE;
        obj->_maximize_dt(*obj, v);
    }
}
void Window::_framebuffer_size_callback(GLFWwindow* o, int w, int h) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        glm::ivec2 size(w, h);
        obj->_fbsize_dt(*obj, size);
    }
}
void Window::_content_scale_callback(GLFWwindow* o, float x, float y) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        glm::vec2 pos(x, y);
        obj->_scale_dt(*obj, pos);
    }
}

void Window::_key_callback(GLFWwindow* o, int key, int scancode, int action, int mods) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        Keys k = static_cast<Keys>(key);
        ButtonState s = static_cast<ButtonState>(action);
        Modifiers m(mods);
        obj->_key_dt(*obj, k, scancode, s, m);
    }
}
void Window::_char_callback(GLFWwindow* o, unsigned int c) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        spec::utf8_codepoint cp = spec::utf8_codepoint::get(c);
        obj->_char_dt(*obj, cp);
    }
}
void Window::_charmods_callback(GLFWwindow* o, unsigned int c, int mods) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        spec::utf8_codepoint cp = spec::utf8_codepoint::get(c);
        Modifiers m(mods);
        obj->_charmods_dt(*obj, cp, m);
    }
}
void Window::_mouse_button_callback(GLFWwindow* o, int button, int action, int mods) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        MouseButtons b = static_cast<MouseButtons>(button);
        ButtonState s = static_cast<ButtonState>(action);
        Modifiers m(mods);
        obj->_mouse_button_dt(*obj, b, s, m);
    }
}
void Window::_cursor_pos_callback(GLFWwindow* o, double xpos, double ypos) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        glm::dvec2 pos(xpos, ypos);
        obj->_cursor_pos_dt(*obj, pos);
    }
}
void Window::_cursor_enter_callback(GLFWwindow* o, int state) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        bool e = state != GLFW_FALSE;
        obj->_cursor_enter_dt(*obj, e);
    }
}
void Window::_scroll_callback(GLFWwindow* o, double xoffset, double yoffset) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        glm::dvec2 offset(xoffset, yoffset);
        obj->_scroll_dt(*obj, offset);
    }
}
void Window::_drop_callback(GLFWwindow* o, int path_count, const char* paths[]) {
    Window* obj = _retrieve_crossreference(o);
    if (obj != nullptr) {
        std::vector<const char*> path_vec;
        path_vec.reserve(path_count);
        for (int i = 0; i < path_count; i++) {
            path_vec.push_back(paths[i]);
        }
        obj->_drop_dt(*obj, path_vec);
    }
}

}  // namespace satr::nui
