#include "GlfwMonitor.hpp"

#include <cstring>

namespace satr::nui {

GLFWvidmode Monitor::VideoMode::get_video_mode() {
    GLFWvidmode r;
    r.width = _width;
    r.height = _height;
    r.redBits = _red_bits;
    r.greenBits = _green_bits;
    r.blueBits = _blue_bits;
    r.refreshRate = _refresh_rate;
    return r;
}

Monitor::GammaRamp::GammaRamp(const GLFWgammaramp* ramp) {
    _red.resize(ramp->size);
    _green.resize(ramp->size);
    _blue.resize(ramp->size);
    std::memcpy(_red.data(), ramp->red, ramp->size * sizeof(unsigned short));
    std::memcpy(_green.data(), ramp->green, ramp->size * sizeof(unsigned short));
    std::memcpy(_blue.data(), ramp->blue, ramp->size * sizeof(unsigned short));
}

GLFWgammaramp Monitor::GammaRamp::get_gamma_ramp() {
    GLFWgammaramp r;
    r.red = _red.data();
    r.green = _green.data();
    r.blue = _blue.data();
    return r;
}

glm::ivec2 Monitor::get_monitor_pos() {
    glm::ivec2 r;
    glfwGetMonitorPos(_handle, &r.x, &r.y);
    glfw_error::check();
    return r;
}

glm::ivec4 Monitor::get_monitor_workarea() {
    glm::ivec4 r;
    glfwGetMonitorWorkarea(_handle, &r.x, &r.y, &r.z, &r.w);
    glfw_error::check();
    return r;
}

glm::ivec2 Monitor::get_monitor_physical_size() {
    glm::ivec2 r;
    glfwGetMonitorPhysicalSize(_handle, &r.x, &r.y);
    glfw_error::check();
    return r;
}

glm::vec2 Monitor::get_monitor_content_scale() {
    glm::vec2 r;
    glfwGetMonitorContentScale(_handle, &r.x, &r.y);
    glfw_error::check();
    return r;
}

const char* Monitor::get_monitor_name() {
    const char* s = glfwGetMonitorName(_handle);
    glfw_error::check();
    return s;
}

std::vector<Monitor::VideoMode> Monitor::get_video_modes() {
    int c;
    const GLFWvidmode* p = glfwGetVideoModes(_handle, &c);
    glfw_error::check();
    std::vector<Monitor::VideoMode> r;
    r.reserve(c);
    for (int i = 0; i < c; i++) {
        Monitor::VideoMode m(&p[i]);
        r.push_back(std::move(m));
    }
    return r;
}
Monitor::VideoMode Monitor::get_video_mode() {
    const GLFWvidmode* p = glfwGetVideoMode(_handle);
    glfw_error::check();
    Monitor::VideoMode r(p);
    return r;
}

void Monitor::set_gamma(float gamma) {
    glfwSetGamma(_handle, gamma);
    glfw_error::check();
}

Monitor::GammaRamp Monitor::get_gamma_ramp() {
    const GLFWgammaramp* p = glfwGetGammaRamp(_handle);
    glfw_error::check();
    Monitor::GammaRamp r(p);
    return r;
}

void Monitor::set_gamma_ramp(GammaRamp& ramp) {
    GLFWgammaramp r = static_cast<GLFWgammaramp>(ramp);
    glfwSetGammaRamp(_handle, &r);
    glfw_error::check();
}

}  // namespace satr::nui
