#ifndef SATR_SPEC_NUI_GLFWMISC_HPP
#define SATR_SPEC_NUI_GLFWMISC_HPP
/**
 * @file
 * @brief Misc structures (Image, Cursor).
 */

#include <meta/Attr.hpp>
#include <Glfw.hpp>

#include <stdexcept>
#include <utility>

namespace satr::nui {

/**
 * Top-left pixel is (0,0) point.
 */
class Image : public spec::INonCopyable {
   public:
    /**
     * The pixels are 32-bit, little-endian, non-premultiplied RGBA.
     */
    packed_struct RGBA {
        uint8_t r = 0;
        uint8_t g = 0;
        uint8_t b = 0;
        uint8_t a = 255;
    };

    class ImageView {
       private:
        Image& _handle;
        int _pos_x;

        ImageView(Image& h, int x) : _handle(h), _pos_x(x) {}

        friend class Image;

       public:
        RGBA& operator[](int y) {
            int pos_y = y;
            glm::ivec2 pos(_pos_x, pos_y);
            return _handle[pos];
        }
    };

   private:
    glm::ivec2 _size;
    RGBA* _data;

   public:
    Image(glm::ivec2 size);

    Image(Image&& o) noexcept :
        _size(std::move(o._size)), _data(std::exchange(o._data, nullptr)) {}
    Image& operator=(Image&& o) noexcept {
        _size = std::move(o._size);
        _data = std::exchange(o._data, nullptr);
        return *this;
    }

    ~Image();

    RGBA& operator[](glm::ivec2 p);

    ImageView operator[](int x) {
        return ImageView(*this, x);
    }

    operator GLFWimage() const {
        GLFWimage r;
        r.width = _size.x;
        r.height = _size.y;
        r.pixels = (decltype(GLFWimage::pixels))(_data);
        return r;
    }
};

class Cursor : public spec::INonCopyable {
   public:
    enum StandardCursors {
        ArrowCursor = GLFW_ARROW_CURSOR,
        IBeamCursor = GLFW_IBEAM_CURSOR,
        CrosshairCursor = GLFW_CROSSHAIR_CURSOR,
        PointingHandCursor = GLFW_POINTING_HAND_CURSOR,
        ResizeEWCursor = GLFW_RESIZE_EW_CURSOR,
        ResizeNSCursor = GLFW_RESIZE_NS_CURSOR,
        ResizeNWSECursor = GLFW_RESIZE_NWSE_CURSOR,
        ResizeNESWCursor = GLFW_RESIZE_NESW_CURSOR,
        ResizeAllCursor = GLFW_RESIZE_ALL_CURSOR,
        NotAllowedCursor = GLFW_NOT_ALLOWED_CURSOR,
    };

   private:
    GLFWcursor* _handle;

    Cursor(GLFWcursor* handle);

   public:
    Cursor();

    Cursor(Cursor&& o) : _handle(std::exchange(o._handle, nullptr)) {}
    Cursor& operator=(Cursor&& o) {
        _handle = std::exchange(o._handle, nullptr);
        return *this;
    }

    ~Cursor();

    friend class Window;

   public:
};

}  // namespace satr::nui

#endif /*SATR_SPEC_NUI_GLFWMISC_HPP*/
