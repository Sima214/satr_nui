#ifndef SATR_SPEC_NUI_GLFWJOYSTICK_HPP
#define SATR_SPEC_NUI_GLFWJOYSTICK_HPP
/**
 * @file
 * @brief C++ RAII style GLFW bindings(groups: input(specific to Joystick and Gamepad)).
 */

namespace satr::nui {

// int joystick_present(int jid);
// const float* get_joystick_axes(int jid, int* count);
// const unsigned char* get_joystick_buttons(int jid, int* count);
// const unsigned char* get_joystick_hats(int jid, int* count);
// const char* get_joystick_name(int jid);
// const char* get_joystick_guid(int jid);
// void set_joystick_user_pointer(int jid, void* pointer);
// void* get_joystick_user_pointer(int jid);
// int joystick_is_gamepad(int jid);
// GLFWjoystickfun set_joystick_callback(GLFWjoystickfun callback);
// int update_gamepad_mappings(const char* string);
// const char* get_gamepad_name(int jid);
// int get_gamepad_state(int jid, GLFWgamepadstate* state);

}  // namespace satr::nui

#endif /*SATR_SPEC_NUI_GLFWJOYSTICK_HPP*/