#ifndef SATR_SPEC_NUI_GLFWWINDOW_HPP
#define SATR_SPEC_NUI_GLFWWINDOW_HPP
/**
 * @file
 * @brief C++ RAII style GLFW bindings(groups: window, input, context, vulkan).
 */

#include <Glfw.hpp>
#include <GlfwMisc.hpp>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <meta/Utils.hpp>
#include <trace/Trace.hpp>
#include <unicode/Unicode.hpp>
#include <vulkan/vulkan.hpp>

#include <chrono>
#include <cstdint>
#include <functional>
#include <optional>
#include <string>
#include <utility>
#include <vector>

namespace satr::nui {

/**
 * Unimplemented hints:
 *  None
 */
class WindowHints {
   public:
    enum ClientApi {
        OpenGL = GLFW_OPENGL_API,
        OpenGLES = GLFW_OPENGL_ES_API,
        None = GLFW_NO_API
    };

    enum ContextCreationApi {
        NativeContextApi = GLFW_NATIVE_CONTEXT_API,
        EglContextApi = GLFW_EGL_CONTEXT_API,
        OsmesaContextApi = GLFW_OSMESA_CONTEXT_API
    };

    enum OpenglProfile {
        OpenGLAnyProfile = GLFW_OPENGL_ANY_PROFILE,
        OpenGLCompatProfile = GLFW_OPENGL_COMPAT_PROFILE,
        OpenGLCoreProfile = GLFW_OPENGL_CORE_PROFILE
    };

    enum ContextRobustness {
        NoRobustness = GLFW_NO_ROBUSTNESS,
        LoseContextOnReset = GLFW_LOSE_CONTEXT_ON_RESET,
        NoResetNotification = GLFW_NO_RESET_NOTIFICATION
    };

    enum ContextReleaseBehavior {
        AnyReleaseBehavior = GLFW_ANY_RELEASE_BEHAVIOR,
        ReleaseBehaviorNone = GLFW_RELEASE_BEHAVIOR_NONE,
        ReleaseBehaviorFlush = GLFW_RELEASE_BEHAVIOR_FLUSH
    };

   private:
    int8_t resizable = -1;
    int8_t visible = -1;
    int8_t decorated = -1;
    int8_t focused = -1;
    int8_t auto_iconify = -1;
    int8_t floating = -1;
    int8_t maximized = -1;
    int8_t center_cursor = -1;
    int8_t transparent_framebuffer = -1;
    int8_t focus_on_show = -1;
    int8_t scale_to_monitor = -1;
    int8_t mouse_passthrough = -1;

    int16_t red_bits = GLFW_DONT_CARE;
    int16_t green_bits = GLFW_DONT_CARE;
    int16_t blue_bits = GLFW_DONT_CARE;
    int16_t alpha_bits = GLFW_DONT_CARE;
    int16_t depth_bits = GLFW_DONT_CARE;
    int16_t stencil_bits = GLFW_DONT_CARE;
    int16_t accum_red_bits = GLFW_DONT_CARE;
    int16_t accum_green_bits = GLFW_DONT_CARE;
    int16_t accum_blue_bits = GLFW_DONT_CARE;
    int16_t accum_alpha_bits = GLFW_DONT_CARE;
    int aux_buffers = GLFW_DONT_CARE;

    int samples = GLFW_DONT_CARE;
    int refresh_rate = GLFW_DONT_CARE;

    int8_t stereo = -1;
    int8_t srgb_capable = -1;
    int8_t doublebuffer = -1;

    ClientApi client_api = ClientApi::OpenGL;
    ContextCreationApi context_creation_api = ContextCreationApi::NativeContextApi;
    int context_version_major = -1;
    int context_version_minor = -1;
    int8_t opengl_forward_compat = -1;
    int8_t opengl_debug_context = -1;
    OpenglProfile opengl_profile = OpenglProfile::OpenGLAnyProfile;
    ContextRobustness context_robustness = ContextRobustness::NoRobustness;
    ContextReleaseBehavior context_release_behavior =
         ContextReleaseBehavior::AnyReleaseBehavior;
    int8_t context_no_error = -1;

    int8_t cocoa_retina_framebuffer = -1;
    std::string cocoa_frame_name;
    int8_t cocoa_graphics_switching = -1;

    int8_t win32_keyboard_menu = -1;

    std::string x11_class_name;
    std::string x11_instance_name;

   public:
    WindowHints() = default;

    void set_resizable(bool v) {
        resizable = v;
    }
    void set_visible(bool v) {
        visible = v;
    }
    void set_decorated(bool v) {
        decorated = v;
    }
    void set_focused(bool v) {
        focused = v;
    }
    void set_auto_iconify(bool v) {
        auto_iconify = v;
    }
    void set_floating(bool v) {
        floating = v;
    }
    void set_maximized(bool v) {
        maximized = v;
    }
    void set_center_cursor(bool v) {
        center_cursor = v;
    }
    void set_transparent_framebuffer(bool v) {
        transparent_framebuffer = v;
    }
    void set_focus_on_show(bool v) {
        focus_on_show = v;
    }
    void set_scale_to_monitor(bool v) {
        scale_to_monitor = v;
    }
    void set_mouse_passthrough(bool v) {
        mouse_passthrough = v;
    }

    void set_red_bits(int v) {
        red_bits = v;
    }
    void set_green_bits(int v) {
        green_bits = v;
    }
    void set_blue_bits(int v) {
        blue_bits = v;
    }
    void set_alpha_bits(int v) {
        alpha_bits = v;
    }
    void set_depth_bits(int v) {
        depth_bits = v;
    }
    void set_stencil_bits(int v) {
        stencil_bits = v;
    }
    void set_accum_red_bits(int v) {
        accum_red_bits = v;
    }
    void set_accum_green_bits(int v) {
        accum_green_bits = v;
    }
    void set_accum_blue_bits(int v) {
        accum_blue_bits = v;
    }
    void set_accum_alpha_bits(int v) {
        accum_alpha_bits = v;
    }
    void set_aux_buffers(int v) {
        aux_buffers = v;
    }

    void set_samples(int v) {
        samples = v;
    }
    void set_refresh_rate(int v) {
        refresh_rate = v;
    }

    void set_stereo(bool v) {
        stereo = v;
    }
    void set_srgb_capable(bool v) {
        srgb_capable = v;
    }
    void set_doublebuffer(bool v) {
        doublebuffer = v;
    }

    void set_client_api(ClientApi v) {
        client_api = v;
    }
    void set_context_creation_api(ContextCreationApi v) {
        context_creation_api = v;
    }
    void set_context_version_major(int v) {
        context_version_major = v;
    }
    void set_context_version_minor(int v) {
        context_version_minor = v;
    }
    void set_opengl_forward_compat(bool v) {
        opengl_forward_compat = v;
    }
    void set_opengl_debug_context(bool v) {
        opengl_debug_context = v;
    }
    void set_opengl_profile(OpenglProfile v) {
        opengl_profile = v;
    }
    void set_context_robustness(ContextRobustness v) {
        context_robustness = v;
    }
    void set_context_release_behavior(ContextReleaseBehavior v) {
        context_release_behavior = v;
    }
    void set_context_no_error(bool v) {
        context_no_error = v;
    }

    void set_cocoa_retina_framebuffer(bool v) {
        cocoa_retina_framebuffer = v;
    }
    void set_cocoa_frame_name(const char* v) {
        cocoa_frame_name = v;
    }
    void set_cocoa_frame_name(std::string& v) {
        cocoa_frame_name = v;
    }
    void set_cocoa_graphics_switching(bool v) {
        cocoa_graphics_switching = v;
    }

    void set_win32_keyboard_menu(bool v) {
        win32_keyboard_menu = v;
    }

    void set_x11_class_name(const char* v) {
        x11_class_name = v;
    }
    void set_x11_class_name(std::string& v) {
        x11_class_name = v;
    }
    void set_x11_instance_name(const char* v) {
        x11_instance_name = v;
    }
    void set_x11_instance_name(std::string& v) {
        x11_instance_name = v;
    }

    void apply() const;

    void reset() const;
};

class Window : public spec::INonCopyable {
   public:
    enum CursorMode {
        Normal = GLFW_CURSOR_NORMAL,
        Hidden = GLFW_CURSOR_HIDDEN,
        Disabled = GLFW_CURSOR_DISABLED
    };

    enum ButtonState { Release = GLFW_RELEASE, Press = GLFW_PRESS, Repeat = GLFW_REPEAT };

    enum MouseButtons {
        Button1 = GLFW_MOUSE_BUTTON_1,
        Button2 = GLFW_MOUSE_BUTTON_2,
        Button3 = GLFW_MOUSE_BUTTON_3,
        Button4 = GLFW_MOUSE_BUTTON_4,
        Button5 = GLFW_MOUSE_BUTTON_5,
        Button6 = GLFW_MOUSE_BUTTON_6,
        Button7 = GLFW_MOUSE_BUTTON_7,
        Button8 = GLFW_MOUSE_BUTTON_8,
        ButtonLast = GLFW_MOUSE_BUTTON_LAST,
        ButtonLeft = GLFW_MOUSE_BUTTON_LEFT,
        ButtonRight = GLFW_MOUSE_BUTTON_RIGHT,
        ButtonMiddle = GLFW_MOUSE_BUTTON_MIDDLE
    };

    struct Modifiers {
        const bool shift : 1;
        const bool control : 1;
        const bool alt : 1;
        const bool super : 1;
        const bool caps_lock : 1;
        const bool num_lock : 1;

        Modifiers(int mods);
    };

    enum Keys {
        Unknown = GLFW_KEY_UNKNOWN,
        Space = GLFW_KEY_SPACE,
        Apostrophe = GLFW_KEY_APOSTROPHE,
        Comma = GLFW_KEY_COMMA,
        Minus = GLFW_KEY_MINUS,
        Period = GLFW_KEY_PERIOD,
        Slash = GLFW_KEY_SLASH,
        _0 = GLFW_KEY_0,
        _1 = GLFW_KEY_1,
        _2 = GLFW_KEY_2,
        _3 = GLFW_KEY_3,
        _4 = GLFW_KEY_4,
        _5 = GLFW_KEY_5,
        _6 = GLFW_KEY_6,
        _7 = GLFW_KEY_7,
        _8 = GLFW_KEY_8,
        _9 = GLFW_KEY_9,
        Semicolon = GLFW_KEY_SEMICOLON,
        Equal = GLFW_KEY_EQUAL,
        A = GLFW_KEY_A,
        B = GLFW_KEY_B,
        C = GLFW_KEY_C,
        D = GLFW_KEY_D,
        E = GLFW_KEY_E,
        F = GLFW_KEY_F,
        G = GLFW_KEY_G,
        H = GLFW_KEY_H,
        I = GLFW_KEY_I,
        J = GLFW_KEY_J,
        K = GLFW_KEY_K,
        L = GLFW_KEY_L,
        M = GLFW_KEY_M,
        N = GLFW_KEY_N,
        O = GLFW_KEY_O,
        P = GLFW_KEY_P,
        Q = GLFW_KEY_Q,
        R = GLFW_KEY_R,
        S = GLFW_KEY_S,
        T = GLFW_KEY_T,
        U = GLFW_KEY_U,
        V = GLFW_KEY_V,
        W = GLFW_KEY_W,
        X = GLFW_KEY_X,
        Y = GLFW_KEY_Y,
        Z = GLFW_KEY_Z,
        LeftBracket = GLFW_KEY_LEFT_BRACKET,
        Backslash = GLFW_KEY_BACKSLASH,
        RightBracket = GLFW_KEY_RIGHT_BRACKET,
        GraveAccent = GLFW_KEY_GRAVE_ACCENT,
        World1 = GLFW_KEY_WORLD_1,
        World2 = GLFW_KEY_WORLD_2,
        Escape = GLFW_KEY_ESCAPE,
        Enter = GLFW_KEY_ENTER,
        Tab = GLFW_KEY_TAB,
        Backspace = GLFW_KEY_BACKSPACE,
        Insert = GLFW_KEY_INSERT,
        Delete = GLFW_KEY_DELETE,
        Right = GLFW_KEY_RIGHT,
        Left = GLFW_KEY_LEFT,
        Down = GLFW_KEY_DOWN,
        Up = GLFW_KEY_UP,
        PageUp = GLFW_KEY_PAGE_UP,
        PageDown = GLFW_KEY_PAGE_DOWN,
        Home = GLFW_KEY_HOME,
        End = GLFW_KEY_END,
        CapsLock = GLFW_KEY_CAPS_LOCK,
        ScrollLock = GLFW_KEY_SCROLL_LOCK,
        NumLock = GLFW_KEY_NUM_LOCK,
        PrintScreen = GLFW_KEY_PRINT_SCREEN,
        Pause = GLFW_KEY_PAUSE,
        F1 = GLFW_KEY_F1,
        F2 = GLFW_KEY_F2,
        F3 = GLFW_KEY_F3,
        F4 = GLFW_KEY_F4,
        F5 = GLFW_KEY_F5,
        F6 = GLFW_KEY_F6,
        F7 = GLFW_KEY_F7,
        F8 = GLFW_KEY_F8,
        F9 = GLFW_KEY_F9,
        F10 = GLFW_KEY_F10,
        F11 = GLFW_KEY_F11,
        F12 = GLFW_KEY_F12,
        F13 = GLFW_KEY_F13,
        F14 = GLFW_KEY_F14,
        F15 = GLFW_KEY_F15,
        F16 = GLFW_KEY_F16,
        F17 = GLFW_KEY_F17,
        F18 = GLFW_KEY_F18,
        F19 = GLFW_KEY_F19,
        F20 = GLFW_KEY_F20,
        F21 = GLFW_KEY_F21,
        F22 = GLFW_KEY_F22,
        F23 = GLFW_KEY_F23,
        F24 = GLFW_KEY_F24,
        F25 = GLFW_KEY_F25,
        KP0 = GLFW_KEY_KP_0,
        KP1 = GLFW_KEY_KP_1,
        KP2 = GLFW_KEY_KP_2,
        KP3 = GLFW_KEY_KP_3,
        KP4 = GLFW_KEY_KP_4,
        KP5 = GLFW_KEY_KP_5,
        KP6 = GLFW_KEY_KP_6,
        KP7 = GLFW_KEY_KP_7,
        KP8 = GLFW_KEY_KP_8,
        KP9 = GLFW_KEY_KP_9,
        KPDecimal = GLFW_KEY_KP_DECIMAL,
        KPDivide = GLFW_KEY_KP_DIVIDE,
        KPMultiply = GLFW_KEY_KP_MULTIPLY,
        KPSubtract = GLFW_KEY_KP_SUBTRACT,
        KPAdd = GLFW_KEY_KP_ADD,
        KPEnter = GLFW_KEY_KP_ENTER,
        KPEqual = GLFW_KEY_KP_EQUAL,
        LeftShift = GLFW_KEY_LEFT_SHIFT,
        LeftControl = GLFW_KEY_LEFT_CONTROL,
        LeftAlt = GLFW_KEY_LEFT_ALT,
        LeftSuper = GLFW_KEY_LEFT_SUPER,
        RightShift = GLFW_KEY_RIGHT_SHIFT,
        RightControl = GLFW_KEY_RIGHT_CONTROL,
        RightAlt = GLFW_KEY_RIGHT_ALT,
        RightSuper = GLFW_KEY_RIGHT_SUPER,
        Menu = GLFW_KEY_MENU,
    };

    using pos_cb = void(Window&, glm::ivec2 pos);
    using size_cb = void(Window&, glm::ivec2 size);
    using close_cb = void(Window&);
    using refresh_cb = void(Window&);
    using focus_cb = void(Window&, bool focused);
    using iconify_cb = void(Window&, bool iconified);
    using maximize_cb = void(Window&, bool maximized);
    using fbsize_cb = void(Window&, glm::ivec2 size);
    using scale_cb = void(Window&, glm::vec2 scale);

    using key_cb = void(Window&, Keys key, int scancode, ButtonState action, Modifiers mods);
    using char_cb = void(Window&, spec::utf8_codepoint c);
    using charmods_cb = void(Window&, spec::utf8_codepoint c, Modifiers mods);
    using mouse_button_cb = void(Window&, MouseButtons button, ButtonState action,
                                 Modifiers mods);
    using cursor_pos_cb = void(Window&, glm::dvec2 pos);
    using cursor_enter_cb = void(Window&, bool entered);
    using scroll_cb = void(Window&, glm::dvec2 offset);
    using drop_cb = void(Window&, const std::vector<const char*>& paths);

   private:
    GLFWwindow* _handle;
    void* _user_data;

    spec::DispatchTable<pos_cb> _pos_dt;
    spec::DispatchTable<size_cb> _size_dt;
    spec::DispatchTable<close_cb> _close_dt;
    spec::DispatchTable<refresh_cb> _refresh_dt;
    spec::DispatchTable<focus_cb> _focus_dt;
    spec::DispatchTable<iconify_cb> _iconify_dt;
    spec::DispatchTable<maximize_cb> _maximize_dt;
    spec::DispatchTable<fbsize_cb> _fbsize_dt;
    spec::DispatchTable<scale_cb> _scale_dt;

    spec::DispatchTable<key_cb> _key_dt;
    spec::DispatchTable<char_cb> _char_dt;
    spec::DispatchTable<charmods_cb> _charmods_dt;
    spec::DispatchTable<mouse_button_cb> _mouse_button_dt;
    spec::DispatchTable<cursor_pos_cb> _cursor_pos_dt;
    spec::DispatchTable<cursor_enter_cb> _cursor_enter_dt;
    spec::DispatchTable<scroll_cb> _scroll_dt;
    spec::DispatchTable<drop_cb> _drop_dt;

    Window(glm::ivec2 size, const char* title, Monitor* monitor, Window* share);

    void _crossreference();
    static Window* _retrieve_crossreference(GLFWwindow* o);

    friend class GLFW;

   public:
    Window(Window&& o) noexcept :
        _handle(std::exchange(o._handle, nullptr)), _pos_dt(std::move(o._pos_dt)),
        _size_dt(std::move(o._size_dt)), _close_dt(std::move(o._close_dt)),
        _refresh_dt(std::move(o._refresh_dt)), _focus_dt(std::move(o._focus_dt)),
        _iconify_dt(std::move(o._iconify_dt)), _maximize_dt(std::move(o._maximize_dt)),
        _fbsize_dt(std::move(o._fbsize_dt)), _scale_dt(std::move(o._scale_dt)),
        _key_dt(std::move(o._key_dt)), _char_dt(std::move(o._char_dt)),
        _charmods_dt(std::move(o._charmods_dt)),
        _mouse_button_dt(std::move(o._mouse_button_dt)),
        _cursor_pos_dt(std::move(o._cursor_pos_dt)),
        _cursor_enter_dt(std::move(o._cursor_enter_dt)), _scroll_dt(std::move(o._scroll_dt)),
        _drop_dt(std::move(o._drop_dt)) {
        _crossreference();
    };
    Window& operator=(Window&& o) noexcept {
        _handle = std::exchange(o._handle, nullptr);
        _pos_dt = std::move(o._pos_dt);
        _size_dt = std::move(o._size_dt);
        _close_dt = std::move(o._close_dt);
        _refresh_dt = std::move(o._refresh_dt);
        _focus_dt = std::move(o._focus_dt);
        _iconify_dt = std::move(o._iconify_dt);
        _maximize_dt = std::move(o._maximize_dt);
        _fbsize_dt = std::move(o._fbsize_dt);
        _scale_dt = std::move(o._scale_dt);
        _key_dt = std::move(o._key_dt);
        _char_dt = std::move(o._char_dt);
        _charmods_dt = std::move(o._charmods_dt);
        _mouse_button_dt = std::move(o._mouse_button_dt);
        _cursor_pos_dt = std::move(o._cursor_pos_dt);
        _cursor_enter_dt = std::move(o._cursor_enter_dt);
        _scroll_dt = std::move(o._scroll_dt);
        _drop_dt = std::move(o._drop_dt);
        _crossreference();
        return *this;
    };
    ~Window();

    bool should_close();
    void set_should_close(bool v);
    void set_title(const char* title);
    void set_title(const std::string& title);
    void set_icons(std::vector<Image> images);
    glm::ivec2 get_pos();
    void set_pos(glm::ivec2 pos);
    glm::ivec2 get_size();
    void set_size_limits(glm::ivec2 min_size, glm::ivec2 max_size);
    void set_aspect_ratio(int numer, int denom);
    void set_size(glm::ivec2 size);
    glm::ivec2 get_framebuffer_size();
    glm::ivec4 get_frame_size();
    glm::vec2 get_content_scale();
    float get_opacity();
    void set_opacity(float opacity);

    bool get_focused();
    bool get_iconified();
    bool get_maximized();
    bool get_hovered();
    bool get_visible();
    bool get_resizable();
    bool get_decorated();
    bool get_auto_iconify();
    bool get_floating();
    bool get_transparent_framebuffer();
    bool get_focus_on_show();
    WindowHints::ClientApi get_client_api();
    WindowHints::ContextCreationApi get_context_creation_api();
    Version get_context_version();

    void set_decorated(bool v);
    void set_resizable(bool v);
    void set_floating(bool v);
    void set_auto_iconify(bool v);
    void set_focus_on_show(bool v);

    void iconify();
    void restore();
    void maximize();
    void show();
    void hide();
    void focus();
    void request_attention();

    std::optional<Monitor> get_monitor();
    void set_monitor(Monitor& monitor, glm::ivec2 pos, glm::ivec2 size, int refresh_rate);
    void set_windowed(glm::ivec2 pos, glm::ivec2 size);

    void set_user_data(void* ptr) {
        _user_data = ptr;
    }
    void* get_user_data() const {
        return _user_data;
    }

    size_t register_position_callback(std::function<pos_cb> callback);
    void unregister_position_callback(size_t uid);
    size_t register_size_callback(std::function<size_cb> callback);
    void unregister_size_callback(size_t uid);
    size_t register_close_callback(std::function<close_cb> callback);
    void unregister_close_callback(size_t uid);
    size_t register_refresh_callback(std::function<refresh_cb> callback);
    void unregister_refresh_callback(size_t uid);
    size_t register_focus_callback(std::function<focus_cb> callback);
    void unregister_focus_callback(size_t uid);
    size_t register_iconify_callback(std::function<iconify_cb> callback);
    void unregister_iconify_callback(size_t uid);
    size_t register_maximize_callback(std::function<maximize_cb> callback);
    void unregister_maximize_callback(size_t uid);
    size_t register_framebuffer_size_callback(std::function<fbsize_cb> callback);
    void unregister_framebuffer_size_callback(size_t uid);
    size_t register_content_scale_callback(std::function<scale_cb> callback);
    void unregister_content_scale_callback(size_t uid);

    void poll_events();
    void wait_events();
    void wait_events_timeout(double timeout);
    template<typename D> void wait_events(D timeout) {
        auto seconds =
             std::chrono::duration_cast<std::chrono::duration<double, std::ratio<1, 1>>>(
                  timeout);
        wait_events_timeout(seconds.count());
    }
    void post_empty_event();

    CursorMode get_input_cursor_mode();
    void set_input_cursor_mode(CursorMode mode);
    bool get_input_sticky_keys_mode();
    void set_input_sticky_keys_mode(bool mode);
    bool get_input_sticky_mouse_buttons_mode();
    void set_input_sticky_mouse_buttons_mode(bool mode);
    bool get_input_lock_keys_mods_mode();
    void set_input_lock_keys_mods_mode(bool mode);
    bool get_input_raw_mouse_motion_mode();
    void set_input_raw_mouse_motion_mode(bool mode);
    bool raw_mouse_motion_supported();

    glm::dvec2 get_cursor_pos();
    void set_cursor_pos(glm::dvec2 pos);
    std::optional<Cursor> create_cursor(const Image& image, glm::ivec2 hotspot);
    std::optional<Cursor> create_cursor(Cursor::StandardCursors shape);
    void set_cursor(Cursor& cursor);

    ButtonState get_mouse_button(MouseButtons button);

    const char* get_key_name(Keys key, int scancode);
    const char* get_key_name(Keys key) {
        return get_key_name(key, -1);
    }
    const char* get_scancode_name(int scancode) {
        return get_key_name(Keys::Unknown, scancode);
    }
    int get_key_scancode(Keys key);
    ButtonState get_key(Keys key);

    void set_clipboard_string(const char* string);
    void set_clipboard_string(const std::string& string) {
        set_clipboard_string(string.c_str());
    }
    const char* get_clipboard_string();

    size_t register_key_callback(std::function<key_cb> cb);
    void unregister_key_callback(size_t uid);
    size_t register_char_callback(std::function<char_cb> cb);
    void unregister_char_callback(size_t uid);
    size_t register_charmods_callback(std::function<charmods_cb> cb);
    void unregister_charmods_callback(size_t uid);
    size_t register_mouse_button_callback(std::function<mouse_button_cb> cb);
    void unregister_mouse_button_callback(size_t uid);
    size_t register_cursor_pos_callback(std::function<cursor_pos_cb> cb);
    void unregister_cursor_pos_callback(size_t uid);
    size_t register_cursor_enter_callback(std::function<cursor_enter_cb> cb);
    void unregister_cursor_enter_callback(size_t uid);
    size_t register_scroll_callback(std::function<scroll_cb> cb);
    void unregister_scroll_callback(size_t uid);
    size_t register_drop_callback(std::function<drop_cb> cb);
    void unregister_drop_callback(size_t uid);

    void make_context_current();
    static void detach_context_current();
    static Window* get_current_context();
    void swap_buffers();
    void swap_interval(int interval);
    bool extension_supported(const char* extension);
    bool extension_supported(const std::string& extension) {
        return extension_supported(extension.c_str());
    }
    static MARK_PURE void* get_proc_address(const char* procname) {
        void* r = (void*) glfwGetProcAddress(procname);
        glfw_error::check();
        return r;
    }

    std::pair<::vk::Result, ::vk::SurfaceKHR> create_window_surface(
         ::vk::Instance& instance, const ::vk::AllocationCallbacks& allocator);
    std::pair<::vk::Result, ::vk::SurfaceKHR> create_window_surface(::vk::Instance& instance);

   protected:
    static void _position_callback(GLFWwindow* o, int x, int y);
    static void _size_callback(GLFWwindow* o, int w, int h);
    static void _close_callback(GLFWwindow* o);
    static void _refresh_callback(GLFWwindow* o);
    static void _focus_callback(GLFWwindow* o, int status);
    static void _iconify_callback(GLFWwindow* o, int status);
    static void _maximize_callback(GLFWwindow* o, int status);
    static void _framebuffer_size_callback(GLFWwindow* o, int w, int h);
    static void _content_scale_callback(GLFWwindow* o, float x, float y);

    static void _key_callback(GLFWwindow* o, int key, int scancode, int action, int mods);
    static void _char_callback(GLFWwindow* o, unsigned int c);
    static void _charmods_callback(GLFWwindow* o, unsigned int c, int mods);
    static void _mouse_button_callback(GLFWwindow* o, int button, int action, int mods);
    static void _cursor_pos_callback(GLFWwindow* o, double xpos, double ypos);
    static void _cursor_enter_callback(GLFWwindow* o, int state);
    static void _scroll_callback(GLFWwindow* o, double xoffset, double yoffset);
    static void _drop_callback(GLFWwindow* o, int path_count, const char* paths[]);
};

}  // namespace satr::nui

#endif /*SATR_SPEC_NUI_GLFWWINDOW_HPP*/