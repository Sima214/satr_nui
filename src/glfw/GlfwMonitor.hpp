#ifndef SATR_SPEC_NUI_GLFWMONITOR_HPP
#define SATR_SPEC_NUI_GLFWMONITOR_HPP
/**
 * @file
 * @brief C++ RAII style GLFW bindings(groups: monitor).
 */

#include <Glfw.hpp>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>

#include <vector>

namespace satr::nui {

class Monitor : public spec::INonCopyable {
   public:
    class VideoMode : public spec::INonCopyable {
       private:
        const int _width;
        const int _height;
        const int _red_bits;
        const int _green_bits;
        const int _blue_bits;
        const int _refresh_rate;

       public:
        VideoMode(const GLFWvidmode* vidmode) :
            _width(vidmode->width), _height(vidmode->height), _red_bits(vidmode->redBits),
            _green_bits(vidmode->greenBits), _blue_bits(vidmode->blueBits),
            _refresh_rate(vidmode->refreshRate) {}

        int get_width() {
            return _width;
        }
        int get_height() {
            return _height;
        }
        int get_red_bits() {
            return _red_bits;
        }
        int get_green_bits() {
            return _green_bits;
        }
        int get_blue_bits() {
            return _blue_bits;
        }
        int get_refresh_rate() {
            return _refresh_rate;
        }

        GLFWvidmode get_video_mode();

        operator GLFWvidmode() {
            return get_video_mode();
        }
    };

    class GammaRamp : public spec::INonCopyable {
       private:
        std::vector<unsigned short> _red;
        std::vector<unsigned short> _green;
        std::vector<unsigned short> _blue;

       public:
        GammaRamp(const GLFWgammaramp*);

        GLFWgammaramp get_gamma_ramp();

        operator GLFWgammaramp() {
            return get_gamma_ramp();
        }
    };

   private:
    GLFWmonitor* const _handle;

    Monitor(GLFWmonitor* handle) : _handle(handle) {}
    friend class GLFW;
    friend class Window;

   public:
    glm::ivec2 get_monitor_pos();
    glm::ivec4 get_monitor_workarea();
    glm::ivec2 get_monitor_physical_size();
    glm::vec2 get_monitor_content_scale();
    const char* get_monitor_name();
    std::vector<VideoMode> get_video_modes();
    VideoMode get_video_mode();
    void set_gamma(float gamma);
    GammaRamp get_gamma_ramp();
    void set_gamma_ramp(GammaRamp& ramp);
};

}  // namespace satr::nui

#endif /*SATR_SPEC_NUI_GLFWMONITOR_HPP*/