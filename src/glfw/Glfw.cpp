#include "GLFW/glfw3.h"
#include "Glfw.hpp"

#include <GlfwMonitor.hpp>
#include <GlfwWindow.hpp>
#include <Trace.hpp>

#include <sstream>

namespace satr::nui {

glfw_error::glfw_error() : std::runtime_error("") {
    // NOTE: slow code path.
    const char* glfw_desc = nullptr;
    int glfw_code = glfwGetError(&glfw_desc);
    set_error_msg(glfw_code, glfw_desc);
}

glfw_error::glfw_error(int glfw_code, const char* glfw_desc) : std::runtime_error("") {
    set_error_msg(glfw_code, glfw_desc);
}

char const* glfw_error::what() const noexcept {
    return description.c_str();
}

void glfw_error::check() {
    const char* glfw_desc = nullptr;
    int glfw_code = glfwGetError(&glfw_desc);
    if (glfw_code != GLFW_NO_ERROR) {
        throw glfw_error(glfw_code, glfw_desc);
    }
}

void glfw_error::set_error_msg(int glfw_code, const char* glfw_desc) {
    code = glfw_code;
    std::stringstream tmp("GLFW Error: ");
    if (glfw_desc == nullptr) {
        tmp << code;
    }
    else {
        tmp << glfw_desc << "(" << code << ")";
    }
    description = tmp.str();
}

Version Version::get_compiled_version() {
    return Version(GLFW_VERSION_MAJOR, GLFW_VERSION_MINOR, GLFW_VERSION_REVISION);
}

Version Version::get_runtime_version() {
    int a, b, c;
    glfwGetVersion(&a, &b, &c);
    // ^- No possible errors.
    return Version(a, b, c);
}

void GLFWHints::apply() {
    if (platform != -1) {
        glfwInitHint(GLFW_PLATFORM, platform);
        glfw_error::check();
    }
    if (joystick_hat_buttons != -1) {
        glfwInitHint(GLFW_JOYSTICK_HAT_BUTTONS, joystick_hat_buttons);
        glfw_error::check();
    }
    if (cocoa_chdir_resources != -1) {
        glfwInitHint(GLFW_COCOA_CHDIR_RESOURCES, cocoa_chdir_resources);
        glfw_error::check();
    }
    if (cocoa_menubar != -1) {
        glfwInitHint(GLFW_COCOA_MENUBAR, cocoa_menubar);
        glfw_error::check();
    }
}

GLFW::GLFW() {
    spec::trace("glfw::loading");
    int status = glfwInit();
    if (!status) {
        throw glfw_error();
    }
    glfwSetMonitorCallback(on_monitor);
    spec::trace("glfw::loaded");
}

GLFW& GLFW::get_instance() {
    static GLFW singleton;
    // spec::trace("glfw::instance");
    return singleton;
}

GLFW::~GLFW() {
    spec::trace("glfw::unloading");
    glfwTerminate();
    spec::trace("glfw::unloaded");
}

bool GLFW::is_platform_supported(GLFWPlatform platform) {
    return glfwPlatformSupported(static_cast<int>(platform)) != GLFW_FALSE;
}
void GLFW::init_vulkan_loader(PFN_vkGetInstanceProcAddr loader) {
    glfwInitVulkanLoader(loader);
}

Version GLFW::get_compiled_version() {
    return Version::get_compiled_version();
}
Version GLFW::get_runtime_version() {
    return Version::get_runtime_version();
}
GLFWPlatform GLFW::get_platform() {
    int platform = glfwGetPlatform();
    return static_cast<GLFWPlatform>(platform);
}

bool GLFW::vulkan_supported() {
    int v = glfwVulkanSupported();
    glfw_error::check();
    return v != GLFW_FALSE;
}
std::vector<const char*> GLFW::get_required_instance_extensions() {
    uint32_t c;
    const char** v = glfwGetRequiredInstanceExtensions(&c);
    glfw_error::check();
    std::vector<const char*> r;
    r.reserve(c);
    for (uint32_t i = 0; i < c; i++) {
        r.push_back(v[i]);
    }
    return r;
}
void* GLFW::get_instance_proc_address(::vk::Instance& instance, const char* procname) {
    VkInstance cinstance = instance;
    void* procaddr = (void*) glfwGetInstanceProcAddress(cinstance, procname);
    glfw_error::check();
    return procaddr;
}
bool GLFW::get_physical_device_presentation_support(::vk::Instance instance,
                                                    ::vk::PhysicalDevice device,
                                                    uint32_t queuefamilyindex) {
    VkInstance cinstance = instance;
    VkPhysicalDevice cdevice = device;
    int v = glfwGetPhysicalDevicePresentationSupport(cinstance, cdevice, queuefamilyindex);
    glfw_error::check();
    return v != GLFW_FALSE;
}

double GLFW::get_time() {
    return glfwGetTime();
    // error check skipped for performance reasons.
}
void GLFW::set_time(double time) {
    glfwSetTime(time);
    glfw_error::check();
}
uint64_t GLFW::get_timer_value() {
    return glfwGetTimerValue();
}
uint64_t GLFW::get_timer_frequency() {
    return glfwGetTimerFrequency();
}

void GLFW::on_monitor(GLFWmonitor* mon, int status) {
    satr::nui::GLFW& instance = satr::nui::GLFW::get_instance();
    Monitor m(mon);
    bool connected;
    if (status == GLFW_CONNECTED) {
        connected = true;
    }
    else if (status == GLFW_DISCONNECTED) {
        connected = false;
    }
    else {
        return;
    }
    instance.monitor_dt(m, connected);
}

std::vector<Monitor> GLFW::get_monitors() {
    int c;
    GLFWmonitor** a = glfwGetMonitors(&c);
    glfw_error::check();
    spec::trace("glfw::get_monitors(", c, ")");
    std::vector<Monitor> r;
    r.reserve(c);
    for (int i = 0; i < c; i++) {
        Monitor v(a[i]);
        r.push_back(std::move(v));
    }
    return r;
}

std::optional<Monitor> GLFW::get_primary_monitor() {
    GLFWmonitor* v = glfwGetPrimaryMonitor();
    glfw_error::check();
    spec::trace("glfw::get_primary_monitor(", v == nullptr, ")");
    if (v != nullptr) {
        return Monitor(v);
    }
    return {};
}

size_t GLFW::register_monitor_callback(std::function<monitor_cb> cb) {
    return monitor_dt.register_callback(cb);
}
void GLFW::unregister_monitor_callback(size_t uid) {
    monitor_dt.unregister_callback(uid);
}

Window GLFW::create_window(glm::ivec2 size, const char* title, Monitor& monitor,
                           const WindowHints& hints, Window& share) {
    hints.apply();
    auto w = Window(size, title, &monitor, &share);
    hints.reset();
    return w;
}
Window GLFW::create_window(glm::ivec2 size, const char* title, Monitor& monitor,
                           const WindowHints& hints) {
    hints.apply();
    auto w = Window(size, title, &monitor, nullptr);
    hints.reset();
    return w;
}
Window GLFW::create_window(glm::ivec2 size, const char* title, Monitor& monitor,
                           Window& share) {
    return Window(size, title, &monitor, &share);
}
Window GLFW::create_window(glm::ivec2 size, const char* title, const WindowHints& hints,
                           Window& share) {
    hints.apply();
    auto w = Window(size, title, nullptr, &share);
    hints.reset();
    return w;
}
Window GLFW::create_window(glm::ivec2 size, const char* title, Monitor& monitor) {
    return Window(size, title, &monitor, nullptr);
}
Window GLFW::create_window(glm::ivec2 size, const char* title, const WindowHints& hints) {
    hints.apply();
    auto w = Window(size, title, nullptr, nullptr);
    hints.reset();
    return w;
}
Window GLFW::create_window(glm::ivec2 size, const char* title, Window& share) {
    return Window(size, title, nullptr, &share);
}
Window GLFW::create_window(glm::ivec2 size, const char* title) {
    return Window(size, title, nullptr, nullptr);
}

Window GLFW::create_window(glm::ivec2 size, const std::string& title, Monitor& monitor,
                           const WindowHints& hints, Window& share) {
    return create_window(size, title.c_str(), monitor, hints, share);
}
Window GLFW::create_window(glm::ivec2 size, const std::string& title, Monitor& monitor,
                           const WindowHints& hints) {
    return create_window(size, title.c_str(), monitor, hints);
}
Window GLFW::create_window(glm::ivec2 size, const std::string& title, Monitor& monitor,
                           Window& share) {
    return create_window(size, title.c_str(), monitor, share);
}
Window GLFW::create_window(glm::ivec2 size, const std::string& title, const WindowHints& hints,
                           Window& share) {
    return create_window(size, title.c_str(), hints, share);
}
Window GLFW::create_window(glm::ivec2 size, const std::string& title, Monitor& monitor) {
    return create_window(size, title.c_str(), monitor);
}
Window GLFW::create_window(glm::ivec2 size, const std::string& title,
                           const WindowHints& hints) {
    return create_window(size, title.c_str(), hints);
}
Window GLFW::create_window(glm::ivec2 size, const std::string& title, Window& share) {
    return create_window(size, title.c_str(), share);
}
Window GLFW::create_window(glm::ivec2 size, const std::string& title) {
    return create_window(size, title.c_str());
}

}  // namespace satr::nui
